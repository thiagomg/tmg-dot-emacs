;;; init.el --- Thiago's emacs initialization file

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives
;;              '("gnu" . "https://melpa.org/packages/") t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (wombat)))
 '(lua-indent-level 4)
 '(make-backup-files nil)
 '(package-selected-packages
   (quote
    (racer company-rust magit rtags cmake-ide cmake-mode format-sql hy-mode mustache-mode markdown-preview-eww markdown-mode go-mode editorconfig editorconfig-generate jinja2-mode json-mode sokoban flymake-lua kotlin-mode flycheck-kotlin cargo rust-mode flycheck-rust flycheck company-lua company d-mode yaml-mode)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Thiago custom --------------------------------------------------

;; Server mode
(require 'server)
(unless (server-running-p) (server-start))

;; Show trailling spaces
(defun toggle-show-trailing-whitespace ()
  "Toggle `show-trailing-whitespace'."
  (interactive)
  (setq show-trailing-whitespace (not show-trailing-whitespace)))

;; Remove trailing whitespaces before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Recent files
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Always spaces instead of tabs :^(
(setq-default indent-tabs-mode nil)

;; Wikipedia-mode
(autoload 'wikipedia-mode "~/.emacs.d/tmg/tmg-wikipedia-mode/wikipedia-mode.el"
 "Major mode for editing documents in Wikipedia markup." t)

;; Multiple-cursors --------------------------------------
(use-package multiple-cursors
  :bind (
	 ("C-S-c C-S-c" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)
	 ("C-S-<mouse-1>" . mc/add-cursor-on-click)
	 )
)

;; Join lines --------------------------------------------
(defun top-join-line ()
  "Join the current line with the line beneath it."
  (interactive)
  (delete-indentation 1))

(global-set-key (kbd "C-M-j") 'top-join-line)
;; -------------------------------------------------------


;; Handling empty lines ----------------------------------

(defun flush-blank-lines (start end)
"Remove blank lines in a region.
START - start line
END - end line"
  (interactive "r") (flush-lines "^\\s-*$" start end nil))

(defun collapse-blank-lines (start end)
"Collapse blank lines in a region.
START - start line
END - end line"
  (interactive "r") (replace-regexp "^\n\\{2,\\}" "\n" nil start end))

;; -------------------------------------------------------

;; magit keybindings -------------------------------------
(use-package magit
  :bind (
         ("C-x g" . magit-status)
         )
  )
;; -------------------------------------------------------

;; MarkDown ----------------------------------------------
(use-package markdown-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (autoload 'gfm-mode "markdown-mode"
    "Major mode for editing GitHub Flavored Markdown files" t)
  (add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
  )

;; -------------------------------------------------------


;; YAML --------------------------------------------------
(use-package yaml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
  (add-hook 'yaml-mode-hook
            '(lambda ()
               (define-key yaml-mode-map "\C-m" 'newline-and-indent)))
  )

;; el-get ------------------------------------------------
;; (unless (require 'el-get nil 'noerror)
;;   (require 'package)
;;   (add-to-list 'package-archives
;;                '("melpa" . "http://melpa.org/packages/"))
;;   (package-refresh-contents)
;;   (package-initialize)
;;   (package-install 'el-get)
;;   (require 'el-get))

;; (add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
;; (el-get 'sync)
;;--------------------------------------------------------

;; flycheck ----------------------------------------------

(use-package flycheck)

;; (use-package rustic)

(use-package rust-mode
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (add-hook 'rust-mode-hook 'cargo-minor-mode)
  (use-package flycheck-rust
    :config
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
  )
;; (with-eval-after-load 'rust-mode
;;   (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
;; -------------------------------------------------------

(use-package flymake-lua
  :config
  (add-hook 'lua-mode-hook 'flymake-lua-load)
  )


(use-package editorconfig
  :config
  (editorconfig-mode 1)
  )

;; ;; Cmake IDE
;; (require 'rtags) ;; optional, must have rtags installed
;; (cmake-ide-setup)

(use-package company
  :config
  (use-package company-lua
    :config
    (defun my-lua-mode-company-init ()
      (setq-local company-backends '((company-lua
                                      company-etags
                                      company-dabbrev-code
                                      company-yasnippet))))
    (add-hook 'lua-mode-hook #'my-lua-mode-company-init)
    )
  )

(use-package racer
  :config
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)

  ;; Don't forget to run:
  ;; rustup component add  rust-src

  ;; Racer environment
  ;; (setq racer-cmd "~/.cargo/bin/racer")
  ;; (setq racer-rust-src-path "~/s/src/3rd/rust/src")

  ;; Auto complete for rust
  (require 'rust-mode)
  (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)

  ;; (add-hook 'rust-mode-hook
  ;;           (lambda ()
  ;;             (local-set-key (kbd "C-c <tab>") #'rust-format-buffer)))

  (setq company-tooltip-align-annotations t)

)

(provide 'init)
